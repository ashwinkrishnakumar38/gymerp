<?php

use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(Database\Seeders\RoleSeeder::class);
        $this->call(Database\Seeders\AdminSeeder::class);
    }
}
