<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WebsiteController extends Controller
{
    public function adminDashboard(){
        return view('pages.admin.dashboard');
    }

    public function trainerDashboard(){
        return view('pages.trainer.dashboard');
    }
}
